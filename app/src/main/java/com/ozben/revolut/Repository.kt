package com.ozben.revolut

import android.util.Log
import com.google.gson.Gson
import com.ozben.revolut.RatesDownloader.Companion.digits
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


interface Repository<T>{
    suspend fun getData(onResponse: suspend (T?) -> Unit)
}

/**
 * A calculator that handles the Base rate value (it is not getting updated by the response from the repository)
 */
class BaseRateCalculator {

    var lastCurrent: Double= 1.0
    var current: Double = 1.0
    var baseRate: Double = 1.0

    fun calc(relative: Double): Double {

        current = relative

        baseRate = current.takeIf { it != 0.0 && lastCurrent != 1.0 }?.run { baseRate * (current / lastCurrent) }
            ?: 1.0

        lastCurrent = current

        return baseRate
    }
}

/**
 * A downloader class for the Rates data from the supplied URL
 * @property base can be changed by the creator and being invoked at the URL
 * @property filter is controlled by the creator and being invoked at the response fetching
 */
class RatesDownloader(val base: String = "EUR", private val filter: Array<String>): Repository<MutableList<Rates.RateData>>{

    companion object {
        const val digits: Int = 2
    }

    private val baseCalculator = BaseRateCalculator()

    override suspend fun getData(onResponse: suspend (MutableList<Rates.RateData>?) -> Unit) {
        onResponse(getRates())
    }

    private suspend fun getRates(): MutableList<Rates.RateData>? {

        return withContext(Dispatchers.IO){

            val url = URL("https://revolut.duckdns.org/latest?base=$base")
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

            connection.connect()

            connection.inputStream?.run {
                val reader = InputStreamReader(this)
                Gson().fromJson(reader, Rates::class.java).run { generateRatesMutableList(rates, filter, baseCalculator) }
            }?: kotlin.run {
                Log.d("Http","Http request failed")
                null
            }

        }
    }
}

data class Rates(val base: String, val date: String, val rates: Map<String, Double>){

    fun generateRatesMutableList(rates: Map<String, Double>, array: Array<String>,baseRateCalculator: BaseRateCalculator): MutableList<RateData> {

        val mutableMutableList = mutableListOf<RateData>()

        array.forEach { rateName ->
            rates[rateName]?.run { mutableMutableList.add(RateData(rateName, this)) }
        }

        return mutableMutableList.apply {
            add(0, RateData(base,  baseRateCalculator.calc(mutableMutableList.takeIf { it.isNotEmpty() }?.get(0)?.rate ?: 1.0) ))
        }
    }

    data class RateData(val name: String, var rate: Double) {

        var value: Double = 0.0
        set(value) {
            field = value.digitsFormat(digits)
        }

        override fun toString(): String {
            return "the rate of $name is -> $rate and its value is -> $value"
        }
    }
}
