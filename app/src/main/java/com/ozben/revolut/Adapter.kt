package com.ozben.revolut

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

class RatesAdapter(private val notifier: Notifier) : RecyclerView.Adapter<RatesViewHolder>(),
    Observer<MutableList<Rates.RateData>> {

    private var ratesList: MutableList<Rates.RateData> = mutableListOf()

    private var watcherController: WatcherController? = null

    private var textWatcher = ViewHolderWatcher { rateData, updatedValue ->
        notifier.onValueUpdated(rateData, updatedValue)
    }

    private val wTextWatcher = WeakReference(textWatcher)

    override fun onChanged(updatedRatesList: MutableList<Rates.RateData>?) {
        updatedRatesList?.run {
            ratesList = this
            ratesList.forEachIndexed { index, _ ->

                if (index != 0) {
                    notifyItemChanged(index)
                }
            }
        }
       // Log.d("rates adapter",updatedRatesList.toString())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.rates_view_holder, parent,false)
        return RatesViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return ratesList.size
    }

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {

        // In order to control the textWatcher when the adapter is at other positions
        if (position == 0) {
            watcherController = holder
        }

        holder.bind(ratesList[position], wTextWatcher) { pos ->
            notifier.onSelected(pos) {
                watcherController?.unregister()
                notifyItemMoved(pos, 0)
            }
        }
    }

    class ViewHolderWatcher(val onValueUpdated: (rateData: Rates.RateData, updatedValue: Double) -> Unit) : TextWatcher {

        var rateData: Rates.RateData? = null
        var currentValue = ""

        override fun afterTextChanged(text: Editable?) {

            if (text?.toString() == currentValue) {
                return
            }

            rateData?.run {
                text?.let { text ->
                    currentValue = text.toString()

                    // Notifies avout text changes at the current watched element
                    onValueUpdated( this, if ( currentValue.isEmpty()) { 0.0 } else { currentValue.toDouble() })
                }
            }
        }

        override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(text: CharSequence?,  start: Int, count: Int, after: Int) {}
    }
}
