package com.ozben.revolut

import kotlin.math.round

fun Double.digitsFormat(numberOfDigits: Int): Double = kotlin.run {
    val digitFactor = 0.1 * 10 * numberOfDigits * 10
    return round(this * digitFactor) / (digitFactor)
}