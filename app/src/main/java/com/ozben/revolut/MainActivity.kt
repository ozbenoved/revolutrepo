package com.ozben.revolut

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var model: RatesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        model = ViewModelProviders.of(this).get(RatesViewModel::class.java).apply {
            repository = RatesDownloader("EUR", arrayOf("CAD", "USD","SEK"))
        }

        rates_recycler_view.layoutManager = LinearLayoutManager(this)

        RatesAdapter(model.notifier).let {ratesAdapter ->

            model.rates.observe(this, ratesAdapter)
            rates_recycler_view.adapter = ratesAdapter
        }

    }

    override fun onDestroy() {
        model.stopTimer()
        super.onDestroy()
    }
}
