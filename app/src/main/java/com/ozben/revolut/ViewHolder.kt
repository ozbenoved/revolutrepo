package com.ozben.revolut

import android.content.Context
import android.content.res.Resources
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rates_view_holder.view.*
import java.lang.ref.WeakReference
import java.util.*

interface WatcherController  {
    fun unregister()
}

open class RatesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), WatcherController{

    private val context: Context = itemView.context
    private val res: Resources = context.resources
    private lateinit var onClick: (position: Int) -> Unit
    private var wTextWatcher: WeakReference<RatesAdapter.ViewHolderWatcher>? = null

    init {
        itemView.run {

            setOnClickListener {
                itemView.valueEditText.requestFocus()
            }

            itemView.valueEditText.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    onClick.invoke(adapterPosition)
                }
            }

        }
    }

    override fun unregister() {
        itemView.valueEditText.removeTextChangedListener(wTextWatcher?.get()?.apply {
            rateData = null
        })
    }

    private fun initText(name: String) {
        itemView.rateNameShort.text = name
        res.getIdentifier(name.toLowerCase(Locale.getDefault()), "string", context.packageName)
            .takeIf { it != 0 }?.run {
            itemView.rateName.text = res.getString(this)
        }
    }

    private fun initImage(name: String) {
        res.getIdentifier(name.toLowerCase(Locale.getDefault()), "drawable", context.packageName).takeIf { it != 0 }?.run {
            itemView.flag.setImageDrawable(res.getDrawable(this, res.newTheme()))
        }
    }

    fun bind(
        rateData: Rates.RateData,
        wTextWatcher: WeakReference<RatesAdapter.ViewHolderWatcher>,
        onClick: (position: Int) -> Unit
    ) {
        this.wTextWatcher = wTextWatcher
        this.onClick = onClick

        updateValue( rateData.value )

        itemView.run {

            rateData.name.run {
                initImage(this)
                initText(this)
            }
        }


        setTextListener(wTextWatcher, rateData)

    }

    private fun setTextListener(textWatcher: WeakReference<RatesAdapter.ViewHolderWatcher>, rateData: Rates.RateData) {
        textWatcher.get()?.run {

            if (adapterPosition == 0) {
                itemView.valueEditText.addTextChangedListener(this.apply {
                    this.rateData = rateData
                })
            } else {
                itemView.valueEditText.removeTextChangedListener(this)
            }
        }
    }

    fun updateValue(value: Double) {
        val valueText = value.toString()

        if (valueText.isEmpty() || valueText == "0.0") {
            itemView.valueEditText.setText("")
        } else {
            itemView.valueEditText.setText(valueText)
        }
    }

}