package com.ozben.revolut

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import java.util.*

class RatesViewModel: ViewModel() {

    lateinit var refreshRatesTimer: RefreshRatesTimer
    var repository: Repository<MutableList<Rates.RateData>>? = null

    var notifier = RatesNotifier()

    // LIve data encapsulation in order to observe the changes at the Adapter directly
    val rates: MutableLiveData<MutableList<Rates.RateData>> by lazy {
        MutableLiveData<MutableList<Rates.RateData>>()
    }

    init {
        callFirstData()
        notifierSetup()
        initTimer()
    }

    /**
     * calls for data update for the first time
     */
    private fun  callFirstData() {
        viewModelScope.launch {
            repository?.getData { dataFromRepo ->
                rates.value = dataFromRepo
                notifier.rates = rates.value
            }
        }
    }

    /**
     * Setup of the notifier callbacks
     */
    private fun notifierSetup() {
        notifier.onReady = { updatedData, callback ->
            viewModelScope.launch {
                rates.value = updatedData
                callback?.invoke()
            }
        }
    }

    /**
     * Initializes the Timer
     */
    private fun initTimer(){

        refreshRatesTimer = RefreshRatesTimer {
            viewModelScope.launch {
                repository?.getData { dataFromRepo ->
                    rates.value = updateRates(dataFromRepo)
                }
            }
        }
        refreshRatesTimer.start()

    }

    /**
     * Update the rates data that are being received regularly by the timer intents
     */
    private fun updateRates(dataFromRepo: MutableList<Rates.RateData>?): MutableList<Rates.RateData>? {

        rates.value?.forEachIndexed{ index, currentRate ->

            // Leave the value of the first line as it is, and update the rest
            dataFromRepo?.first { currentRate.name == it.name }?.run {

                 if (index != 0) {
                     currentRate.value = currentRate.value * currentRate.rate / rate
                 }

                 currentRate.rate = rate
            }

            Log.d("updateRates", "UPDATED ${currentRate.name} , VALUE: ${currentRate.value}")

        }

       // Log.d("updateRates","${rates.value}")

        return rates.value

    }

    class RatesNotifier : Notifier{

        var rates: MutableList<Rates.RateData>? = null
        var onReady: ((MutableList<Rates.RateData>?, callback: (() -> Unit)?) -> Unit)? = null

        /**
         * Being notified when an item has been selected at the List.
         * Move the item to the top of the list
         */
        override fun onSelected(position: Int, callback: () -> Unit) {
            rates?.takeIf { position > 0 }?.let { rates ->
                val rate = rates[position]
                var temp: Rates.RateData?
                var p = position
                while (p > 0) {
                    temp = rates[p - 1]
                    rates[p - 1] = rates[p]
                    rates[p] = temp
                    p -= 1
                }
                onValueUpdated(rate, rate.value, callback)
            }

   //         Log.d("Notifier","SELECTED $position")
        }

        /**
         * Being notified when the first item's value has been updated.
         * Calculates the relative change of the values and updates the list
         */
        override fun onValueUpdated(rateData: Rates.RateData, value: Double, callback: (() -> Unit)?) {

            rates?.forEachIndexed { index, currentRate->

                currentRate.value = value * currentRate.rate / rateData.rate
                Log.d("Notifier", "UPDATED ${currentRate.name} , VALUE: ${currentRate.value}")
            }

            onReady?.invoke(rates, callback)


      //      Log.d("Notifier", "UPDATED $rateData , VALUE: $value")
        }

    }

    fun stopTimer() {
        refreshRatesTimer.stop()
    }

}

/**
 * A custom Timer class that will invoke the analytics upload every defined time
 */
class RefreshRatesTimer(val onUpdate: () -> Unit): Timer() {

    private var stopLoop = false

    fun stop() {
        stopLoop = true
        //    Log.d("Timer", "Timer has finished")

    }

    fun start() {

        if (!stopLoop) {

        //    Log.d("Timer", "Timer has started")

            schedule(object : TimerTask() {

                override fun run() {

                    onUpdate()
                    start()

                }

            }, 2000)
        }
    }
}

interface Notifier{
    fun onSelected(position: Int, callback: () -> Unit)
    fun onValueUpdated(rateData: Rates.RateData, value: Double, callback: (() -> Unit)? = null)
}